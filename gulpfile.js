// Plugins
const
  gulp         = require('gulp'),
  sourcemaps   = require('gulp-sourcemaps'),
  
  csso         = require('csso')
  stylus       = require('gulp-stylus'),
  postcss      = require('gulp-postcss'),
  autoprefixer = require('autoprefixer'),
  stylelint    = require('stylelint'),
  sorting      = require('postcss-sorting'),
  
  pug          = require('gulp-pug');


// Config
const
  cfg = require('gulpconfig.json')


gulp.task('styles', () => {

  return gulp
    .src(cfg.styles.source)
    .pipe( concat('style.styl') )
    .pipe( stylus() )
    .pipe( gulp.dest(cfg.styles.build) )

});


gulp.task('default', gulp.series('pages', 'styles');